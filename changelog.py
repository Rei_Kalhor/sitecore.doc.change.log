import auxilaryfunctions as af
import argparse
import logging

exception_template = "An exception of type {0} occurred. Arguments:\n{1!r}. Function: {2}"
logging.basicConfig(filename="logging.log",
                    level=logging.ERROR,
                    format='%(asctime)s | %(name)s | %(levelname)s | %(message)s')

if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument("--confluenceurl", help="Elasticsearch address", type=str, required=True)
        parser.add_argument("--confluenceusername", help="Elasticsearch user", type=str, required=True)
        parser.add_argument("--confluencepassword", help="Elasticsearch password", type=str, required=True)
        parser.add_argument("--pageid", help="Elasticsearch password", type=str, required=True)
        parser.add_argument("--bitbucketurl", help="Documents Directory", type=str, required=True)
        parser.add_argument("--bitbucketusername", help="Documents Directory", type=str, required=True)
        parser.add_argument("--bitbucketpassword", help="elastic search index", type=str, required=True)
        parser.add_argument("--workspace", help="elastic search index", type=str, required=True)
        parser.add_argument("--repositoryname", help="elastic search index", type=str, required=True)
        args = parser.parse_args()

        confluence_url = args.confluenceurl
        confluence_username = args.confluenceusername
        confluence_password = args.confluencepassword
        page_id = args.pageid

        af.initialize_confluence(confluence_url, confluence_username, confluence_password)
        page_info = af.get_page_info(page_id)
        last_date = af.get_last_date_of_inserted_commits(page_info=page_info)

        bitbucket_url = args.bitbucketurl
        bitbucket_username = args.bitbucketusername
        bitbucket_password = args.bitbucketpassword
        bitbucket_workspace = args.workspace
        bitbucket_repository = args.repositoryname

        bitbucket_encoded_auth = af.encode_auth(bitbucket_username, bitbucket_password)
        bitbucket_basic_auth = "Basic {0}".format(bitbucket_encoded_auth.decode())

        commits = af.get_commits_from_bitbucket(bitbucket_url=bitbucket_url,
                                                basic_auth=bitbucket_basic_auth,
                                                workspace=bitbucket_workspace,
                                                repository_name=bitbucket_repository,
                                                date_filter=last_date)
        if len(commits) > 0:
            response = af.write_comments_in_atlassian_doc(page_info=page_info, comments=commits)
            print(response)
        else:
            print("No new commits found...")

    except Exception as ex:
        logging.error(exception_template.format(ex.args[0], ex.args[1], ex.args[2]))



    #     confluence_url = 'https://stylelabs.atlassian.net'
    #     confluence_username = "kre@sitecore.net"
    #     confluence_password = "wFC3NzYWtT739qNAg6Ng4828"
    #     page_id = 2523004929
    #
    #     af.initialize_confluence(confluence_url, confluence_username, confluence_password)
    #     page_info = af.get_page_info(page_id)
    #     last_date = af.get_last_date_of_inserted_commits(page_info=page_info)
    #
    #
    #     bitbucket_username = "Rei_Kalhor"
    #     bitbucket_password = "jGKKeJ9vq25T2zyvSBmG"
    #     bitbucket_url = "https://api.bitbucket.org/2.0/repositories"
    #     bitbucket_workspace = "Rei_Kalhor"
    #     bitbucket_repository = "sitecore.sample.documents.ingestion"
    #
    #     bitbucket_encoded_auth = af.encode_auth(bitbucket_username, bitbucket_password)
    #     bitbucket_basic_auth = "Basic {0}".format(bitbucket_encoded_auth.decode())
    #
    #     commits = af.get_commits_from_bitbucket(bitbucket_url=bitbucket_url,
    #                                             basic_auth=bitbucket_basic_auth,
    #                                             workspace=bitbucket_workspace,
    #                                             repository_name=bitbucket_repository,
    #                                             date_filter=last_date)
    #     if len(commits) > 0:
    #         response = af.write_comments_in_atlassian_doc(page_info=page_info, comments=commits)
    #         print(response)
    #     else:
    #         print("No new commits found...")
    #
    # except Exception as ex:
    #     logging.error(exception_template.format(ex.args[0], ex.args[1], ex.args[2]))


