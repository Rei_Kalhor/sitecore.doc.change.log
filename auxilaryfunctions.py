import json
import requests
import base64
import re
from atlassian import Confluence
import itertools
import html2text
from datetime import datetime
import inspect
import logging

confluence = {}
exception_template = "An exception of type {0} occurred in {1} function. Arguments:\n{2!r}"
logging.basicConfig(filename="logging.log",
                    level=logging.ERROR,
                    format='%(asctime)s | %(name)s | %(levelname)s | %(message)s')


def who_am_i():
    try:
        function_name = inspect.stack()[1][3]
    except Exception as ex:
        function_name = "who_am_i"
    finally:
        return function_name


def initialize_confluence(url, username, password):
    global confluence
    try:
        confluence = Confluence(
            url=url,
            username=username,
            password=password)
    except Exception as ex:
        confluence = {}
        raise Exception(type(ex).__name__, ex.args, who_am_i())


def clean_html(raw_html):
    try:
        cleaner = re.compile('<.*?>')
        clean_text = re.sub(cleaner, '', raw_html)
        return clean_text
    except Exception as ex:
        raise Exception(type(ex).__name__, ex.args, who_am_i())


def get_commits_from_bitbucket(bitbucket_url, basic_auth, workspace, repository_name, date_filter):
    commits = []
    try:
        commits_filtered_by_date = []
        format_str = '%Y-%m-%d %H:%M:%S'
        api_url = "{0}/{1}/{2}/commits".format(bitbucket_url, workspace, repository_name)
        header = {
            "Authorization": basic_auth,
            "Content-Type": "application/json"
        }
        response = requests.get(url=api_url, headers=header)

        if response.status_code == 200:
            response_json = json.loads(response.text)
            response_values = response_json['values']

            if date_filter == "":
                commits_filtered_by_date = response_values
            else:
                for commit in response_values:
                    if datetime.strptime(commit["date"][0:19].replace("T", " "), format_str) > date_filter:
                        commits_filtered_by_date.append(commit)

            for commit in commits_filtered_by_date:
                hash_commit = commit["hash"]
                comment_url = "{0}/{1}/{2}/commit/{3}".format(bitbucket_url,
                                                              workspace,
                                                              repository_name,
                                                              hash_commit)

                commit_response = requests.get(url=comment_url, headers=header)
                if commit_response.status_code == 200:
                    commit_response_json = json.loads(commit_response.text)
                    comment = {
                        "message": commit_response_json["message"].replace('\n', ' '),
                        "author": commit_response_json["author"]["user"]["display_name"],
                        "author_nickname": commit_response_json["author"]["user"]["nickname"],
                        "author_email": re.search('<(.*)>', commit_response_json["author"]["raw"]).group(1),
                        "date": commit_response_json["date"][0:10]
                    }
                    commits.append(comment)
                else:
                    logging.error("The commit by hash code {0} could not be retrieved from bitbucket, error occurred "
                                  "at function {1}".format(hash_commit, who_am_i()))
        else:
            logging.error("The commits could not be retrieved from bitbucket, error occurred at function {0}".format(
                who_am_i()))
    except Exception as ex:
        commits = []
        logging.error(exception_template.format(type(ex).__name__, who_am_i(), ex.args))
    finally:
        return commits


def encode_auth(username, password):
    try:
        temp = username + ":" + password
        temp_bytes = temp.encode('ascii')
        base64_bytes = base64.b64encode(temp_bytes)
    except Exception as ex:
        base64_bytes = ""
        logging.error(exception_template.format(type(ex).__name__, who_am_i(), ex.args))
    finally:
        return base64_bytes


def get_page_info(page_id):
    try:
        total_info = confluence.get_page_by_id(page_id)
        history = confluence.history(page_id)
        last_modified_date = history["lastUpdated"]["when"][0:19].replace("T", " ")
        return {
            "id": total_info["id"],
            "type": total_info["type"],
            "space": total_info["space"]["key"],
            "title": total_info["title"],
            "last_modified_date": last_modified_date
        }
    except Exception as ex:
        raise Exception(type(ex).__name__, ex.args, who_am_i())


def write_comments_in_atlassian_doc(page_info, comments):
    try:
        group_key = lambda x: x['date']
        content = ""
        for key, group in itertools.groupby(comments, group_key):
            content = content + key + ":\n\n"
            groups = (list(group))
            for grp in groups:
                content = content + "Author: @{0} ({1}) {2}\nCommit: {3} \n\n".format(grp["author_nickname"],
                                                                                      grp["author"],
                                                                                      grp["author_email"],
                                                                                      grp["message"])
        response = confluence._insert_to_existing_page(page_id=page_info["id"],
                                                       title=page_info["title"],
                                                       insert_body=content,
                                                       top_of_page=True)
        return response
    except Exception as ex:
        raise Exception(type(ex).__name__, ex.args, who_am_i())


def get_page_content(page_info):
    try:
        page_body = confluence.get_page_as_word(page_info["id"]).decode("utf-8").replace("\n", " ").replace("\r", " ")
        h = html2text.HTML2Text()
        h.ignore_links = True
        page_content = h.handle(page_body).replace("\n", "")
        return page_content
    except Exception as ex:
        raise Exception(type(ex).__name__, ex.args, who_am_i())


def get_last_date_of_inserted_commits(page_info):
    try:
        page_title = page_info["title"]
        page_content = get_page_content(page_info)
        index = page_content.find(page_title)
        temp_index = index + len(page_title)
        if not page_content.__contains__("Commit"):
            last_modified_date = ""
        else:
            temp = page_content[temp_index:temp_index + 10]
            datetime.strptime(temp, '%Y-%m-%d')
            last_modified_date = datetime.strptime(page_info["last_modified_date"], '%Y-%m-%d %H:%M:%S')
        return last_modified_date
    except Exception as ex:
        raise Exception(type(ex).__name__, ex.args, who_am_i())
